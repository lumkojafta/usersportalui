import { Component, EventEmitter, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-add-new-user',
  templateUrl: './add-new-user.component.html',
  styleUrls: ['./add-new-user.component.css']
})
export class AddNewUserComponent implements OnInit {

  addNewPostForm: FormGroup;
  categories: any[] = [];
  event: EventEmitter<any>=new EventEmitter();

  constructor(private builder: FormBuilder, private userService: UserService, private bsModalRef: BsModalRef) { 
    // this.addNewPostForm = this.builder.group({
    //   category: new FormControl(null, []),
    //   title: new FormControl('', []),
    //   description: new FormControl('', [])
    // }); 

    this.addNewPostForm = new FormGroup({
      'first_Name': new FormControl(null),
      'last_Name': new FormControl(null),
      'date_Of_Birth': new FormControl(null ),
      'email_Address': new FormControl(null),
      'address': new FormControl(null),
      'city': new FormControl(null),
      'zip_Code': new FormControl(null),
    });

    this.userService.getCategoryList().subscribe(data => {
      Object.assign(this.categories, data);
    }, error => { console.log('Error while gettig category data.'); });
  }

  onPostFormSubmit(){
    this.addNewPostForm.value.id = 0;     
    this.userService.addPost(this.addNewPostForm.value).subscribe((data: any)=>{
      console.log(data);
      if(data!=null && data>0){
        this.event.emit('OK');
        this.bsModalRef.hide();
      }
    });
  }

  onClose(){
    this.bsModalRef.hide();
  }


  ngOnInit(): void {
    
  }

}
