import { Component } from '@angular/core';
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { AddNewUserComponent } from './add-new-user/add-new-user.component';
import { UserService } from './service/user.service';
import { ViewPostComponent } from './view-post/view-post.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'UserPortal';
  postList: any[] = [];
  bsModalRef: BsModalRef;
  
  //title = 'UserPortal';
  constructor(private userService: UserService, private bsModalService: BsModalService) {
    this.getPosts();
  }
  

  getPosts() {
    this.userService.getPostList().subscribe(data => {
      Object.assign(this.postList, data);
      console.log("postList", this.postList);
    }, error => {
      console.log("Error while getting posts ", error);
    });
  }

  editPost(postId: any) {
    this.userService.changePostId(postId);

    this.bsModalRef = this.bsModalService.show(ViewPostComponent);
    this.bsModalRef.content.event.subscribe((result : any) => {
      if (result == 'OK') {
        setTimeout(() => {
          this.getPosts();
        }, 5000); 
      }
    });
  }
  addNewPost() {
    this.bsModalRef = this.bsModalService.show(AddNewUserComponent);
    this.bsModalRef.content.event.subscribe((result: any) => {
      if (result == 'OK') {
        this.getPosts();
      }
    });
  }
}
