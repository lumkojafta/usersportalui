import { Component, OnInit, Input, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-view-post',
  templateUrl: './view-post.component.html',
  styleUrls: ['./view-post.component.css']
})
export class ViewPostComponent implements OnInit {
 
  editPostForm: FormGroup;
  categories: any[] = [];
  postId: number;
  postData: any;
  event: EventEmitter<any> = new EventEmitter();

  constructor(private builder: FormBuilder, private userService: UserService, private bsModalRef: BsModalRef) {
   
    this.editPostForm = this.builder.group({
      first_Name: new FormControl(null, []),
      last_Name: new FormControl('', []),
      email_Address: new FormControl('', []),
      date_Of_Birth: new FormControl('', []),
      city: new FormControl('', []),
      address: new FormControl('', []),
      zip_Code: new FormControl('', [])      
    });

    // this.userService.getCategoryList().subscribe(data => {
    //   Object.assign(this.categories, data);
    // }, error => { console.log('Error while gettig category data.'); });

    this.userService.postIdData.subscribe((data: any) => {
      this.postId = data;   
           


          if (data!=null) {            
            this.editPostForm.controls['first_Name'].setValue(data.first_Name);
            this.editPostForm.controls['last_Name'].setValue(data.last_Name);
            this.editPostForm.controls['email_Address'].setValue(data.email_Address);
            this.editPostForm.controls['date_Of_Birth'].setValue(data.date_Of_Birth);
            this.editPostForm.controls['city'].setValue(data.city);
            this.editPostForm.controls['address'].setValue(data.address);
            this.editPostForm.controls['zip_Code'].setValue(data.zip_Code);
          }
       // }, error => { console.log("Error while gettig post details") });
      });
   // });
   }
   onPostEditFormSubmit() {
    // let postData = {
    //   'PostId': this.postId,
    //   'Title': this.editPostForm.get('title').value,
    //   'Description': this.editPostForm.get('description').value,
    //   'CategoryId': this.editPostForm.get('category').value,
    // };

    // this.editPostForm = new FormGroup({
    //   'first_Name': new FormControl(null),
    //   'last_Name': new FormControl(null),
    //   'date_Of_Birth': new FormControl(null ),
    //   'email_Address': new FormControl(null),
    //   'address': new FormControl(null),
    //   'city': new FormControl(null),
    //   'zip_Code': new FormControl(null),
    // });

    // this.userService.updatePost(this.editPostForm.value).subscribe((data) => {      
    //     this.event.emit('OK');
    //     this.bsModalRef.hide();      
    // });
  }
   onClose(){
    this.bsModalRef.hide();
  }

  ngOnInit(): void {
  }

}
