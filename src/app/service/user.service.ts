import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { BehaviorSubject } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private readonly baseURL: string;
  postList: any[] = [];
  postIdSource = new  BehaviorSubject<number>(0);
  postIdData: any;

  constructor(private http: HttpClient) {
    //this.baseURL ="https://localhost:44314/api/post/";
    this.baseURL="https://localhost:44358/api/";
    this.postIdData= this.postIdSource.asObservable();
   }
   getCategoryList(){
    let header = new HttpHeaders();
    header.append('Content-Type', 'applications/json');
    return this.http.get(this.baseURL + "getcategories", { headers: header})
}
getPostList(){
  let header = new HttpHeaders();
  header.append('Content-Type', 'applications/json');
  return this.http.get(this.baseURL + "UserDetail", { headers: header})
}
changePostId(postId: number){
  this.postIdSource.next(postId);
}
getPost(){
  let header = new HttpHeaders();
  header.append('Content-Type', 'applications/json');
  return this.http.get(this.baseURL + "UserDetail", { headers: header})
}

addPost(post: any){
  let header = new HttpHeaders();
  header.append('Content-Type', 'applications/json');
  return this.http.post(this.baseURL + "UserDetail", post, { headers: header})
}
}
