import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from '@angular/common/http';
import { ModalModule, BsModalService } from 'ngx-bootstrap/modal';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ViewPostComponent } from './view-post/view-post.component';

import { UserService } from 'src/app/service/user.service';
import { AddNewUserComponent } from './add-new-user/add-new-user.component';



@NgModule({
  declarations: [
    AppComponent,
    ViewPostComponent,
    AddNewUserComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserModule,

    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    ModalModule.forRoot()
  ],
  providers: [UserService, BsModalService ],
  bootstrap: [AppComponent],
  entryComponents:[ViewPostComponent]
})
export class AppModule { }
